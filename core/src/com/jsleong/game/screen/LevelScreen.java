package com.jsleong.game.screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.jsleong.game.actor.BaseActor;
import com.jsleong.game.actor.Explosion;
import com.jsleong.game.actor.Rock;
import com.jsleong.game.actor.Spaceship;

public class LevelScreen extends BaseScreen {
    private Spaceship spaceship;
    public boolean gameOver;

    @Override
    public void initialize() {
        BaseActor space = new BaseActor(0, 0, mainStage);
        space.loadTexture("space.png");
        space.setSize(800, 600);
        BaseActor.setWorldBounds(space);

        spaceship = new Spaceship(400, 300, mainStage);

        new Rock(600, 500, mainStage);
        new Rock(600, 300, mainStage);
        new Rock(600, 100, mainStage);
        new Rock(400, 100, mainStage);
        new Rock(200, 100, mainStage);
        new Rock(200, 300, mainStage);
        new Rock(200, 500, mainStage);
        new Rock(400, 500, mainStage);

        gameOver = false;
    }


    @Override
    public void update(float dt) {
        for (BaseActor rockActor : BaseActor.getList(mainStage, "com.jsleong.game.actor.Rock")){
            if( rockActor.overlaps(spaceship)) {
                if (spaceship.shieldPower <= 0) {
                    Explosion boom = new Explosion(0,0, mainStage);
                    boom.centerAtActor(spaceship);
                    spaceship.remove();
                    spaceship.setPosition(-1000,-100,0);

                    BaseActor messageLose = new BaseActor(0, 0, uiStage);
                    messageLose.loadTexture("message-lose.png");
                    messageLose.setOpacity(0);
                    messageLose.addAction(Actions.fadeIn(1));
                    gameOver = true;
                }
                else {
                    spaceship.shieldPower -= 34;
                    Explosion boom = new Explosion(0,0, mainStage);
                    boom.centerAtActor(spaceship);
                    rockActor.remove();
                }
            }

            for (BaseActor laserActor : BaseActor.getList(mainStage, "com.jsleong.game.actor.Laser")){
                if (laserActor.overlaps(rockActor)){
                    Explosion boom = new Explosion(0,0,mainStage);
                    boom.centerAtActor(rockActor);
                    laserActor.remove();
                    rockActor.remove();
                }
            }
        }

        if (!gameOver && BaseActor.count(mainStage, "com.jsleong.game.actor.Rock") == 0){
            BaseActor messageWin = new BaseActor(0, 0, uiStage);
            messageWin.loadTexture("message-win.png");
            messageWin.centerAtPosition(400, 300);
            messageWin.setOpacity(0);
            messageWin.addAction(Actions.fadeIn(1));
            gameOver = true;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.SPACE){
            spaceship.shoot();
        }

        if( keycode == Input.Keys.X ){
            spaceship.warp();
        }
        return false;
    }
}
