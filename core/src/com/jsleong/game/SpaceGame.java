package com.jsleong.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jsleong.game.screen.LevelScreen;

public class SpaceGame extends BaseGame {

	@Override
	public void create() {
		super.create();
		this.setActiveScreen(new LevelScreen());
	}
}
